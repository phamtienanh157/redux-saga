import { createStore, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'
import rootSaga from "../sagas/rootSaga"
import myReducer from "../reducers/index"
import { composeWithDevTools } from 'redux-devtools-extension';

// Creates a Redux middleware and connects the Sagas to the Redux Store
const sagaMiddleware = createSagaMiddleware()

// mount it on the Store
const store = createStore(
    myReducer,
    composeWithDevTools(applyMiddleware(sagaMiddleware))
)
// then run the saga
sagaMiddleware.run(rootSaga)

export default store