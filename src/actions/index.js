import * as actionTypes from './actionTypes';

export const getUser = () => ({
    type: actionTypes.GET_USER_REQUEST
})

export const getUserSuccess = (data) => ({
    type: actionTypes.GET_USER_SUCCESS,
    data: data
})

export const getUserFailure = (error) => ({
    type: actionTypes.GET_USER_FAILURE,
    error: error
})

export const getCard = () => ({
    type: actionTypes.GET_CARD_REQUEST
})

export const getCardSuccess = (data) => ({
    type: actionTypes.GET_CARD_SUCCESS,
    data: data
})

export const getCardFailure = (error) => ({
    type: actionTypes.GET_USER_FAILURE,
    error: error
})