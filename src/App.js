import { useDispatch } from "react-redux";
import { getUser, getCard } from "./actions/index"
import './App.css';

function App() {
  const dispatch = useDispatch()

  const onGetUser = () => {
    dispatch(getUser())
  }

  const onGetCard = () => {
    dispatch(getCard())
  }

  return (
    <div className="App">
      <button onClick={onGetUser} type="button">get users</button>
      <button onClick={onGetCard} type="button">get cards</button>
    </div>
  );
}

export default App;
