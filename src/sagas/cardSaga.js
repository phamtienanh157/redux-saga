import axios from "axios"
import { takeLatest, call, put } from 'redux-saga/effects';
import { GET_CARD_REQUEST } from '../actions/actionTypes';
import * as action from "../actions/index"

const getCardApi = () => {
    return axios.get('https://60becf8e6035840017c17a48.mockapi.io/api/cards');
}

function* getCard() {
    try {
        const response = yield call(getCardApi)
        yield put(action.getCardSuccess(response.data))
    } catch (error) {
        yield put(action.getCardFailure(error.message))
    }
}

export function* watchGetCard() {
    yield takeLatest(GET_CARD_REQUEST, getCard);
}
