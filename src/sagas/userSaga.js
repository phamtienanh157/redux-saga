import axios from "axios"
import { takeLatest, call, put } from 'redux-saga/effects';
import { GET_USER_REQUEST } from '../actions/actionTypes';
import * as action from "../actions/index"

const getUserApi = () => {
    return axios.get('https://60becf8e6035840017c17a48.mockapi.io/api/users');
}

function* getUser() {
    try {
        const response = yield call(getUserApi)
        yield put(action.getUserSuccess(response.data))
    } catch (error) {
        yield put(action.getUserFailure(error.message))
    }
}

export function* watchGetUser() {
    yield takeLatest(GET_USER_REQUEST, getUser);
}
