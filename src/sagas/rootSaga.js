import { all } from 'redux-saga/effects';
import { watchGetUser } from './userSaga'
import { watchGetCard } from './cardSaga'


export default function* rootSaga() {
    yield all([
        watchGetUser(),
        watchGetCard()
    ]);
}