import { combineReducers } from 'redux';
import userReducer from './userReducer';
import cardReducer from "./cardReducer";

const myReducer = combineReducers({
    userReducer, cardReducer
});

export default myReducer;