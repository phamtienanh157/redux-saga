import * as actionTypes from '../actions/actionTypes';

const initialState = {
    loading: false,
    error: "",
    users: [],
}

const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.GET_USER_REQUEST:
            return {
                ...state, loading: true
            }
        case actionTypes.GET_USER_SUCCESS:
            return {
                ...state, users: action.data, loading: false
            }
        case actionTypes.GET_USER_FAILURE:
            return {
                ...state, error: action.error, loading: false
            }
        default:
            return state;
    }
}

export default userReducer;