import * as actionTypes from '../actions/actionTypes';

const initialState = {
    loading: false,
    error: "",
    cards: [],
}

const cardReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.GET_CARD_REQUEST:
            return {
                ...state, loading: true
            }
        case actionTypes.GET_CARD_SUCCESS:
            return {
                ...state, cards: action.data, loading: false
            }
        case actionTypes.GET_CARD_FAILURE:
            return {
                ...state, error: action.error, loading: false
            }
        default:
            return state;
    }
}

export default cardReducer;